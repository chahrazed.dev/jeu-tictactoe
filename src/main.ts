let gameGrid: string[] = [];
let currentPlayer: string = "X";
let gameResult: string = "";

const cellules = document.querySelectorAll<HTMLElement>(".cell");
const replayBtn = document.querySelector<HTMLButtonElement>("#replayBtn");

function startGame() {
    gameGrid = ["", "", "", "", "", "", "", "", ""];
    currentPlayer = "X";
    gameResult = "";
    document.querySelector("#player").textContent = currentPlayer;
    document.querySelector("#result").textContent = gameResult;

    cellules.forEach(cell => {
        cell.textContent = "";
    });

    replayBtn.style.display = "none";
}

function handleClick(e: Event) {
    const clickedCell = e.target as HTMLElement;
    const index = parseInt(clickedCell.dataset.index);
    if (!gameGrid[index] && !gameResult) {
        clickedCell.textContent = currentPlayer;
        gameGrid[index] = currentPlayer;
        checkResult();
        if (!gameResult) {
            currentPlayer = currentPlayer == "X" ? "O" : "X";
            document.querySelector("#player").textContent = currentPlayer;
        }
    }
}

function handleReplay() {
    startGame();
}

cellules.forEach(cell => {
    cell.addEventListener("click", handleClick);
});

replayBtn.addEventListener("click", handleReplay);

function checkResult() {
    const winningGame = [
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
        [0, 4, 8],
        [2, 4, 6],
        [0, 3, 6],
        [1, 4, 7],
        [2, 5, 8]
    ];

    let draw = gameGrid.includes("");
    let winner: string | undefined;

    winningGame.forEach(lines => {
        const [a, b, c] = lines;
        if (gameGrid[a] && gameGrid[a] == gameGrid[b] && gameGrid[a] == gameGrid[c]) {
            winner = gameGrid[a];
        }
    });

    if (winner) {
        gameResult = `Le joueur ${winner} a gagné !`;
    } else if (!draw) {
        gameResult = "Match nul!";
    }

    if (winner || !draw) {
        replayBtn.style.display = "block";
    }

    document.querySelector<HTMLElement>("#result").textContent = gameResult;
}

window.onload = startGame;
